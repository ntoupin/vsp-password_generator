﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PasswordGenerator
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
            
        {
            string procedureToDo = 'Z'.ToString();
            string continueOrNot = 'Y'.ToString();
            string password = "Error";

            while (String.Equals(procedureToDo, '1'.ToString()) ^ !String.Equals(procedureToDo, '2'.ToString()))
            {
                Console.WriteLine("Password (1) ir NIP (2) ?");
                procedureToDo = Console.ReadLine().ToUpper();
            }

            Console.WriteLine();

            if (String.Equals(procedureToDo, '1'.ToString()))
            {
                while (String.Equals(continueOrNot, 'Y'.ToString()))
                {
                    Console.WriteLine("How many characters ?");
                    string numberOfChars = Console.ReadLine();
                    while (!IsDigitsOnly(numberOfChars))
                    {
                        Console.WriteLine("How many characters ?");
                        numberOfChars = Console.ReadLine();
                    }
                    password = CreatePassword(Convert.ToInt16(numberOfChars));
                    Console.WriteLine(password);
                    continueOrNot = 'Z'.ToString();
                    while (String.Equals(continueOrNot, 'Y'.ToString()) ^ !String.Equals(continueOrNot, 'N'.ToString()))
                    {
                        Console.WriteLine("Again ? (Y/N)");
                        continueOrNot = Console.ReadLine().ToUpper();
                    }

                    Console.WriteLine();
                }

                Console.WriteLine("Your final password is : " + password);
                Clipboard.SetText(password);
            }

            if (String.Equals(procedureToDo, '2'.ToString()))
            {
                while (String.Equals(continueOrNot, 'Y'.ToString()))
                {
                    Console.WriteLine("How many numbers ?");
                    string numberOfChars = Console.ReadLine();
                    while (!IsDigitsOnly(numberOfChars))
                    {
                        Console.WriteLine("How many numbers ?");
                        numberOfChars = Console.ReadLine();
                    }
                    password = CreateNIP(Convert.ToInt16(numberOfChars));
                    Console.WriteLine(password);
                    continueOrNot = 'Z'.ToString();
                    while (String.Equals(continueOrNot, 'Y'.ToString()) ^ !String.Equals(continueOrNot, 'N'.ToString()))
                    {
                        Console.WriteLine("Again ? (Y/N)");
                        continueOrNot = Console.ReadLine().ToUpper();
                    }

                    Console.WriteLine();
                }

                Console.WriteLine("Your final NIP is : " + password);
                Clipboard.SetText(password);
            }

            Console.ReadLine();
        }

        static public string CreatePassword(int length)
        {
            const string lowCaseLetters = "abcdefghijklmnopqrstuvwxyz";
            const string highCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string numbers = "1234567890";
            const string signs = "+-*/$%?&()!";

            StringBuilder res = new StringBuilder();
            Random rnd = new Random();

            int n1 = 0, n2 = 0, n3 = 0, n4 = 0;

            while ((n1 + n2 + n3 + n4) != length)
            {
                Random rndNum = new Random();

                n1 = rndNum.Next(1, length);
                n2 = rndNum.Next(1, length);
                n3 = rndNum.Next(1, length);
                n4 = rndNum.Next(1, length);
            }

            while (0 < n1--)
            {
                res.Append(lowCaseLetters[rnd.Next(lowCaseLetters.Length)]);
            }

            while (0 < n2--)
            {
                res.Append(highCaseLetters[rnd.Next(highCaseLetters.Length)]);
            }

            while (0 < n3--)
            {
                res.Append(numbers[rnd.Next(numbers.Length)]);
            }

            while (0 < n4--)
            {
                res.Append(signs[rnd.Next(signs.Length)]);
            }

            string rand = new string(res.ToString().ToCharArray().OrderBy(s => (rnd.Next(2) % 2) == 0).ToArray());

            return rand;
        }

        static public string CreateNIP(int length)
        {
            const string numbers = "1234567890";

            StringBuilder res = new StringBuilder();
            Random rnd = new Random();

            for (int i = 0; i < length; ++i)
            {
                res.Append(numbers[rnd.Next(numbers.Length)]);
            }

            string rand = new string(res.ToString().ToCharArray().OrderBy(s => (rnd.Next(2) % 2) == 0).ToArray());

            return rand;
        }

        static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }
    }
}
